package INF101.lab2;

import java.lang.annotation.Target;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;


    
    public static void main(String[] args) {
        // Have two pokemon fight until one is defeated

        pokemon1 = new Pokemon("Pikachu");
        pokemon2 = new Pokemon("Oddish");

        System.out.println(pokemon1.toString());
        System.out.println(pokemon2.toString());
        
        System.out.println();

        while(true){
            pokemon1.attack(pokemon2);
            pokemon2.attack(pokemon1);
            if(pokemon1.isAlive()==false || pokemon2.isAlive()==false){
                break;
            }
        }
        


       

    }
}
